# Kotlin Spring ORM Hibernate

### Run Locally

```
git clone https://gitlab.com/hendisantika/kotlin-spring-orm-hibernate.git
```

```
mvn clean spring-boot:run
```

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Add New Data

![Add New Data](img/add.png "Add New Data")

List All Data

![List All Data](img/list.png "List All Data")