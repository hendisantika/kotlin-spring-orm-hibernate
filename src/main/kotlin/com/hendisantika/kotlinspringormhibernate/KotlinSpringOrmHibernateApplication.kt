package com.hendisantika.kotlinspringormhibernate

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Controller
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@SpringBootApplication
class KotlinSpringOrmHibernateApplication

@Entity
data class User(@get: Id var id: Number = 0,
                @get: Column(name = "first_name") var firstName: String = "",
                @get: Column(name = "last_name") var lastName: String = "",
                @get: Column(name = "email") var email: String = "",
                @get: Column(name = "phone") var phone: String = "")

@Repository
//class IndexRepository(@Autowired var sessionFactory: SessionFactory) {
interface IndexRepository : CrudRepository<User, Number> {

//    fun getCurrentSession() = sessionFactory.currentSession!!

//    fun addUser(user: User) = getCurrentSession().save(user)!!

//    @Suppress("UNCHECKED_CAST")
//    fun allUsers(): List<User> = getCurrentSession().createCriteria(User::class.java).list().toList() as List<User>
}

@Service
@Transactional
class IndexService(@Autowired var indexRepository: IndexRepository) {

    //    fun addUser(user: User) = indexRepository.addUser(user)
    fun addUser(user: User) = indexRepository.save(user)

    //    fun allUsers(): List<User> = indexRepository.allUsers()
    fun allUsers(): Iterable<User> = indexRepository.findAll()
}

@Controller
@RequestMapping("/")
class IndexController(@Autowired var indexService: IndexService) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doGet(model: Model): String {
        model.apply {
            addAttribute("user", User())
            addAttribute("allUsers", indexService.allUsers())
        }
        return "index"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun doPost(model: Model, user: User): String {
        indexService.addUser(user)
        model.apply {
            addAttribute("user", User())
            addAttribute("allUsers", indexService.allUsers())
        }
        return "index"
    }
}


fun main(args: Array<String>) {
    runApplication<KotlinSpringOrmHibernateApplication>(*args)
}
