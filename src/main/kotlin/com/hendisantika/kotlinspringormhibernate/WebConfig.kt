package com.hendisantika.kotlinspringormhibernate

import org.springframework.context.annotation.Configuration

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-spring-orm-hibernate
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-26
 * Time: 17:23
 */

@Configuration
class WebConfig {
//    @Bean(name = arrayOf("dataSource"))
//    fun dataSource(): DataSource = EmbeddedDatabaseBuilder()
//            .setType(EmbeddedDatabaseType.HSQL)
//            .addScript("schema.sql")
//            .build()
//
//
//    @Bean
//    fun sessionFactory(@Qualifier("dataSource") dataSource: DataSource): LocalSessionFactoryBean {
//        val properties = Properties()
////        properties.setProperty("dialect", "org.hibernate.dialect.HSQLDB")
//        properties.setProperty("dialect", "org.hibernate.dialect.H2Dialect")
//
//        val sessionFactory = LocalSessionFactoryBean().apply {
//            setDataSource(dataSource)
//            setPackagesToScan(*arrayOf("com.hendisantika.kotlinspringormhibernate"))
//
//        }
//
//        sessionFactory.hibernateProperties = properties
//        return sessionFactory
//    }
//
//    //Translate Hibernate Exceptions into Spring One
//    @Bean
//    fun persistenceTranslation(): BeanPostProcessor = PersistenceExceptionTranslationPostProcessor()
}